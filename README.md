# Spring Boot Ktor Starter

#### 项目介绍
本项目用于快速搭建基于Kotlin协程、SpringBoot作为依赖注入、Ktor作为web框架的高性能异步的应用程序，本项目目标力求保持Spring编程风格的优雅及Ktor的高效。
欢迎提交代码，共同创作一个可开箱即用的、优雅高效的Ktor Starter。


#### 软件架构
不损失Ktor原有功能前提下保持Spring风格,尽量使ktor透明


#### 截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/1204/012928_e3e4e88b_1522755.png "QQ浏览器截图20181204012743.png")



#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)