package net.ybrj.springKtor.core

import io.ktor.application.Application

/**
 * 可ktor模块化
 * created by zpf on 2018-12-02
 */
interface KtorModule {

    /**
     * 注册模块
     */
    fun Application.register()
}