package net.ybrj.springKtor.core

import io.ktor.routing.Routing

/**
 * 可ktor路由
 * created by zpf on 2018-12-02
 */
interface KtorRouter {

    /**
     * 路由功能
     */
    fun Routing.route()
}