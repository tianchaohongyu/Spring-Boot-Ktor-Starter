package net.ybrj.springKtor.core.util

import io.ktor.application.ApplicationCall
import io.ktor.routing.Route
import io.ktor.routing.Routing
import io.ktor.routing.route
import io.ktor.util.pipeline.ContextDsl
import io.ktor.util.pipeline.PipelineInterceptor


/**
 * 路由拓展
 * created zpf on 2018/12/3 0003
 */

/**
 * 针对某个路径所有类型的请求
 * @param path 拦截路径
 * @param body 执行代码
 */
@ContextDsl
fun Routing.request(path: String, body: PipelineInterceptor<Unit, ApplicationCall>): Route {
    return route(path) { handle(body) }
}