package net.ybrj.test

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

/**
 * spring ktor 应用容器
 * created by zpf on 2018-12-03
 */
@SpringBootApplication
class SpringKtorApplication

fun main(args: Array<String>) {
    runApplication<SpringKtorApplication>(*args)
}