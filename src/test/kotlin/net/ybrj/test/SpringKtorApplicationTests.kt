package net.ybrj.test

import io.ktor.http.HttpMethod
import io.ktor.server.testing.TestApplicationEngine
import io.ktor.server.testing.handleRequest
import net.ybrj.test.configuration.TestConfiguration
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import
import org.springframework.test.context.junit4.SpringRunner
import javax.annotation.Resource
import kotlin.test.assertEquals

/**
 * spring ktor 单元测试
 * created by zpf on 2018-12-03
 */
@RunWith(SpringRunner::class)
@Import(TestConfiguration::class)
@SpringBootTest(classes = [SpringKtorApplication::class])
open class SpringKtorApplicationTests {

    /**测试应用引擎*/
    @Resource
    private lateinit var testApplicationEngine: TestApplicationEngine

    /**
     * 单元测试
     */
    @Test
    open fun test() {
        testApplicationEngine.apply {
            handleRequest(HttpMethod.Get, "/").apply {
                assertEquals(200, response.status()?.value)
                assertEquals("""{"code":"001","msg":"操作成功。"}""", response.content)
            }
        }
    }
}
