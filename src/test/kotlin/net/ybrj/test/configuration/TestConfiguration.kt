package net.ybrj.test.configuration

import io.ktor.server.engine.ApplicationEngine
import io.ktor.server.engine.ApplicationEngineFactory
import io.ktor.server.testing.TestEngine
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * 测试配置类
 * created by zpf on 2018-12-03
 */
open class TestConfiguration {

    /**
     * 测试引擎工厂
     */
    @Bean
    open fun engineFactory(): ApplicationEngineFactory<ApplicationEngine, out ApplicationEngine.Configuration> {
        return TestEngine
    }
}