package net.ybrj.test.controller

import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.Routing
import net.ybrj.springKtor.core.KtorRouter
import net.ybrj.springKtor.core.util.request
import org.springframework.stereotype.Controller


/**
 * 测试控制器
 * created zpf on 2018/12/3 0003
 */
@Controller
open class TestController : KtorRouter {

    override fun Routing.route() {
        request("/") {
            call.respond(mapOf("code" to "001", "msg" to "操作成功。"))
        }
    }
}