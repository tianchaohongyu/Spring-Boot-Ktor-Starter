package net.ybrj.test.module

import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.jackson.jackson
import net.ybrj.springKtor.core.KtorModule
import org.springframework.stereotype.Component


/**
 * 测试模块
 * created zpf on 2018/12/3 0003
 */
@Component
open class TestModule : KtorModule {

    override fun Application.register() {
        install(ContentNegotiation) {
            jackson {

            }
        }
    }
}